import { deleteLocalStorage, playSoundEffect } from "./commonRoutines.js";
import Rectangle from "./graphModule.js"

// Abstract class
class Page {
    constructor({ key, title }) {
        this.pageElement = document.querySelector(`#content`)
        this.title = title
        this.key = key
    }

    render() {
        return ``
    }

    pageShow() {
        this.pageElement.innerHTML = this.render()
        document.title = this.title
    }

    pageHide() {
        this.pageElement.innerHTML = ''
    }
}

// Landing page, only static, for represantation
export class PageLanding extends Page {
    constructor(settings) {
        super(settings)

        this.log = [];
    }

    render() {
        return `
        <section class="row-center">
            <img class="landing_img" src="img/money.png" alt="Landing img">
            <h2 class="landing-text">Save your money, with ease</h2>
        </section>
        `
    }

    triggerHeadingTransition() {
        // reflow trick https://gist.github.com/paulirish/5d52fb081b3570c81e3a
        document.querySelector(".landing-text").offsetHeight
        document.querySelector(".landing-text").classList.add("ft-3")
    }

    pageShow() {
        super.pageShow()

        this.triggerHeadingTransition()
    }
}

// File page, page for data import/export 
export class PageFile extends Page {
    constructor(settings) {
        super(settings)
    }

    render() {
        return `
            <section>
                <form id="form" class="col">
                    <h4>Export file with expenses</h4>
                    <img id="file" class="transform rotate" src="img/file.png">
                    <div class="row-left">
                        <input id="export" type="submit" value="Export">
                    </div>
                </form>   
                <form id="form" class="col">
                    <h4>Import file with expenses</h4>
                    <div class="row-left">
                        <label for="input-file">Select file in csv format to upload data</label>
                    </div>
                    <div class="row-left">
                        <input id="input-file" type="file" accept=".csv" autofocus required></input>
                    </div>
                    <div class="row-left">
                        <input id="import" type="submit" value="Import">
                    </div>
                <form>
            </section>
        `
    }

    exportFile(e) {
        e.preventDefault()

        playSoundEffect()

        let filename = "data.csv"
        let data = localStorage.getItem("expenses")

        if (data === null) {
            alert("Cannot export data, there are no expenses")
            return
        }

        data = this.createCSVFromData(JSON.parse(data))

        let element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
        element.setAttribute('download', filename);
        element.classList.add("download")

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.querySelector(".download").remove()
    }

    setExportFileListener() {
        document.querySelector("#export").addEventListener("click", this.exportFile.bind(this))
    }

    setImportFileListener() {
        document.querySelector("#import").addEventListener("click", this.importFile.bind(this))
    }

    importFile(e) {
        e.preventDefault()

        const file = document.querySelector("#input-file").files[0]
        const reader = new FileReader();
        reader.readAsText(file)

        reader.onload = function (e) {
            let data = reader.result
            let lines = data.split('\n')
            let expenses = []
            lines.forEach((line) => {
                if (line !== "") {
                    let tokens = line.split(',')
                    let expense = { name: tokens[0].trim(), cost: tokens[1].trim(), date: tokens[2].trim() }
                    expenses.push(expense)
                }
            })
            localStorage.setItem("expenses", JSON.stringify(expenses))
            alert("Data imported")
        };
    }

    createCSVFromData(data) {
        let csvResult = ""
        data.forEach((expense) => {
            csvResult += expense.name + ", " + expense.cost + ", " + expense.date + "\n"
        })
        return csvResult
    }


    pageShow() {
        super.pageShow()

        this.setExportFileListener()
        this.setImportFileListener()

        const form = document.querySelector("#form");
        //inputFile.addEventListener("submit", this.onDrop);
    }

    pageHide() {
        super.pageHide()
    }

    onSubmit(e) {
        e.preventDefault()
        // TODO
    }
}

// Dashboard page, display graph and some addtional info
export class PageDashboard extends Page {
    constructor(settings) {
        super(settings)

        this.rectangle = null
    }

    render() {
        // gap between days is 40
        return `
            <section>
                <article>
                    <h2>Expense graph</h2>

                    <label for="window">Chose a time window</label>
                    <select name="window" id="window">
                        <option value="last-week">Last week</option>
                        <option value="last-7">Last seven days</option>
                    </select>

                    <div class="row-left">
                        <svg id="graph" viewbox="0 0 270 100" width="100%" preserveAspectRatio="xMidYMid meet">
                            <line x1="0" y1="90" x2="300" y2="90" stroke="black" stroke-width="1"/>
                        </svg>
                    </div>
                </article>
            

                <article>
                    <h2>Additional information</h2>

                    <div class="row-around dashboard-line">
                        <div>
                            <h3>Overall expense</h3>
                            <p id="overall-expense"></p>
                        </div>
                        <div class="ms-2">
                            <h3>Biggest expense</h3>
                            <p id="biggest-expense"></p>
                        </div>
                        <div class="ms-2">
                            <h3>Day of biggest expense</h3>
                            <p id="day-expense"></p>
                        </div>
                        <div class="ms-2">
                            <h3>Average expense</h3>
                            <p id="average-expense"></p>
                        </div>
                    </div>
                </article>
            </section>
            `
    }

    /*
    <text x="5" y="96" class="small">Monday</text>
    <text x="45" y="96" class="small">Tuesday</text>
    <text x="85" y="96" class="small">Wednesday</text>
    <text x="125" y="96" class="small">Thursday</text>
    <text x="165" y="96" class="small">Friday</text>
    <text x="205" y="96" class="small">Saturday</text>
    <text x="245" y="96" class="small">Sunday</text>
    */

    renderDashboard() {
        if (localStorage.getItem("expenses") !== null) {
            document.querySelector("#overall-expense").innerText = this.calculateOverallExpense()
            document.querySelector("#biggest-expense").innerText = this.getBiggestExpense()
            document.querySelector("#day-expense").innerText = this.getDayOfBiggestExpense()
            document.querySelector("#average-expense").innerText = this.calculateAverageExpense()
        }
    }

    calculateAverageExpense() {
        return Math.round(this.calculateOverallExpense() / JSON.parse(localStorage.getItem("expenses")).length)
    }

    calculateOverallExpense() {
        let expenses = JSON.parse(localStorage.getItem("expenses"))
        let sum = 0
        if (expenses !== null) {
            expenses.forEach((expense) => {
                sum += parseInt(expense.cost)
            })
        }
        return sum
    }

    getBiggestExpense() {
        let expenses = JSON.parse(localStorage.getItem("expenses"))
        let maxExpense = 0
        if (expenses !== null) {
            expenses.forEach((expense) => {
                maxExpense = Math.max(maxExpense, expense.cost)
            })
        }
        return maxExpense

    }

    getDayOfBiggestExpense() {
        let expenses = JSON.parse(localStorage.getItem("expenses"))
        let maxExpense = this.getBiggestExpense()
        let result
        if (expenses !== null) {
            expenses.forEach((expense) => {
                if (parseInt(expense.cost) === maxExpense) {
                    result = expense.date
                    return
                }
            })
        }
        return result
    }

    registerTimeWindowEventListener() {
        document.querySelector("#window").addEventListener("change", this.rectangle.redraw.bind(this.rectangle))
    }

    pageShow() {
        super.pageShow()

        if (this.rectangle == null) {
            this.rectangle = new Rectangle("#graph")
        }
        this.rectangle.redraw()
        this.renderDashboard()
        this.registerTimeWindowEventListener()
    }
}

// Page for adding expenses and removing them to, form, list of expenses
export class PageExpenses extends Page {
    constructor(settings) {
        super(settings)
    }

    pageShow() {
        super.pageShow()

        this.registerForm()
        this.renderAllExpenses()
        this.registerDeleteAllExpensesListener()
    }

    render() {
        return `
        <section>
            <article>
                <div class="row-left">
                    <h3>Expense form</h3>
                </div>
                <div class="row-left">
                    <button id="delete-all">Delete all stored expenses</button>
                </div>
                <form id="form" class="col">
                    <div class="row-left">
                        <label for="name">Name of expense</label>
                    </div>
                    <div class="row-left">
                        <input id="name" type="text" placeholder="New boots" autofocus required></input>
                    </div>
                    <div class="row-left">
                        <label for="cost">Cost of expense</label>
                    </div>
                    <div class="row-left">
                        <input id="cost" type="number" placeholder="1500" step="1" required></input>
                    </div>
                    <div class="row-left">
                        <label for="date">Date of expense</label>
                    </div>
                    <div class="row-left">
                        <input id="date" type="date" required></input>
                    </div>
                    <div class="row-left">
                        <input id="submit" type="submit" value="Submit">
                    </div>
                </form>
            </article>

            <article>
                <h3>Currently stored expenses</h3>
                <div class="expense-list">
                    <table>
                        
                    </table>
                </div>
            </article>
        </section>
        `
    }

    saveExpenseToStorage() {
        playSoundEffect()

        const expense = {
            name: document.querySelector("#name").value,
            cost: document.querySelector("#cost").value,
            date: document.querySelector("#date").value
        }
        let expenses = JSON.parse(localStorage.getItem("expenses"))
        if (expenses === null) {
            expenses = []
        }
        expenses.push(expense)
        localStorage.setItem("expenses", JSON.stringify(expenses));

        this.renderAllExpenses()
    }

    registerForm() {
        document.querySelector("#submit").addEventListener("click", this.onSubmit.bind(this))
    }

    onSubmit(e) {
        e.preventDefault()
        this.saveExpenseToStorage()
    }

    renderAllExpenses() {
        // clear table, table head is also deleted
        document.querySelector("table").innerHTML = ""
        // setup head
        document.querySelector("table").innerHTML = `
                                                    <tr>
                                                    <th>Name</th>
                                                    <th>Cost</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                                    </tr>
                                                `

        let expenses = JSON.parse(localStorage.getItem("expenses"))

        if (expenses !== null) {
            expenses.forEach((expense) => {
                let tr = document.createElement("tr")
                tr.innerHTML = `
                                <td>${expense.name}</td>
                                <td>${expense.cost}</td>
                                <td>${expense.date}</td>
                                <td><button id="${expense.name}" class="del">Delete</button></td>
                        `
                document.querySelector("tbody").appendChild(tr)
            })
        }

        document.querySelectorAll("button[class='del']").forEach((button) => {
            button.addEventListener("click", this.deleteExpense.bind(this))
        })
    }

    deleteExpense(element) {
        element.preventDefault()

        let expenses = JSON.parse(localStorage.getItem("expenses"))
        expenses.forEach((expense, index) => {
            if (expense.name === element.target.id) {
                expenses.splice(index, 1)
            }
        })

        localStorage.setItem("expenses", JSON.stringify(expenses))

        this.renderAllExpenses()

    }

    registerDeleteAllExpensesListener() {
        document.querySelector("#delete-all").addEventListener("click", deleteLocalStorage.bind(this))
    }
}

// Page contains simple HTML audio, video
export class PageAbout extends Page {
    render() {
        return `
        <div class="row-center">
            <figure>
                <figcaption>Information about application in audio version (AI generated)</figcaption>
                <audio controls src="./audio/about.mp3"></audio>
            </figure>
        </div>
        <div class="row-center">
            <p>How to add expense</p>
        </div>
        <div class="row-center">
            <video controls>
                <source src="./video/manual.mp4"
                type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'
                />
            </video>
        </div>
        `
    }
}

export class PageNotFound extends Page {
    render() {
        return `
            <div class="row-center">
                <h1>404 Page not found</h1>
            </div>
        `
    }
}

