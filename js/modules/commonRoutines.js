export function deleteLocalStorage(e) {
    localStorage.clear()
    this.renderAllExpenses()
}

// load one time, loading each time causes delay
let soundEffect = null
export function playSoundEffect() {
    if (soundEffect !== null) {
        soundEffect.play()
    } else {
        soundEffect = new Audio("./audio/effect.wav");
        soundEffect.play()
    }
}

// Helper method, helps with text centering
export function getWidthOfSVGText(text) {
    let svgNs = "http://www.w3.org/2000/svg";

    const svgText = document.createElementNS(svgNs, "text");
    svgText.setAttributeNS(null, "x", 0);
    svgText.setAttributeNS(null, "y", 0);
    svgText.innerHTML = text
    svgText.classList.add("hidden")
    svgText.classList.add("small")

    document.querySelector("#graph").appendChild(svgText);
    let bbox = svgText.getBBox()

    svgText.remove()
    return bbox.width
}

// Last full week
export function getLastWeekDates() {
    const today = new Date();

    // Set the date to the start of today (midnight)
    today.setHours(23, 0, 0, 0);

    // Calculate the first date of last week
    const startDate = new Date(today);
    startDate.setDate(today.getDate() - 7 - ((today.getDay() + 6) % 7));

    return get7DaysDatesFromStartDay(startDate);
}

function get7DaysDatesFromStartDay(startDate) {
    const last7DaysDates = [];
    // Loop to get each date of the last week
    for (let i = 0; i < 7; i++) {
        const date = new Date(startDate);
        date.setDate(startDate.getDate() + i);

        const dateISOString = date.toISOString();
        // Extract only the date part
        const dateWithoutTime = dateISOString.split('T')[0];

        last7DaysDates.push(dateWithoutTime);
    }

    return last7DaysDates;
}

// Today is included
export function getLast7DaysDates() {
    const today = new Date();

    // Set the date to the start of today (midnight)
    today.setHours(23, 0, 0, 0);

    // Calculate the date of 7 days ago
    const startDate = new Date(today);
    startDate.setDate(today.getDate() - 6);

    return get7DaysDatesFromStartDay(startDate);
}

function handle(e) {
    if (e.target.innerHTML === "Show menu") {
        e.target.innerHTML = "Hide menu"
        document.querySelector("ul").classList.add("visible")
    } else {
        e.target.innerHTML = "Show menu"
        document.querySelector("ul").classList.remove("visible")
    }

}

function menuShowOrHide(x) {
    if (x.matches) { // If media query matches
        document.querySelector(".toggler").addEventListener("click", handle)
    } else {
    }
}

export function initMobileNav() {
    // Create a MediaQueryList object
    var x = window.matchMedia("(max-width: 700px)")

    // Call listener function at run time
    menuShowOrHide(x);

    // Attach listener function on state changes
    x.addEventListener("change", function () {
        menuShowOrHide(x);
    });
}

