import { getLast7DaysDates, getLastWeekDates, getWidthOfSVGText } from "./commonRoutines.js";

// Class that handles graph generation
export default class Graph {
    constructor(target) {
        this.target = target
        this.svg = document.querySelector(target);
        this.svgNs = "http://www.w3.org/2000/svg";
    }

    reInit(){
        this.svg = document.querySelector(this.target);
    }

    clearSVG(){
        document.querySelectorAll("#graph text, #graph rect").forEach((element) => {
            element.remove()
        })
    }

    redraw() {
        this.reInit()
        this.clearSVG()
        
        let currentX = 5
        let baselineY = 89.5
        let width = 20

        let timeWindow = document.querySelector("#window").value

        let data = this.getExpenseData(timeWindow)
        let maxExpense = data.maxExpense
        data = data.data
    
        let stepY = 50 / maxExpense
        for (let i = 0; i < 7; i++) {
            let day = data[i]
            if (day != 0) {
                let height = day * stepY
                this.drawRectangle(currentX, baselineY - height, width, height)
                let projectedTextWidth = getWidthOfSVGText(day)
                this.drawText((20 - projectedTextWidth) / 2 + currentX, baselineY - height - 5, day)
            }
            currentX += 40
        }
    }

    generateSVGDays(lastWeek){
        let currentX = 5
        let baselineY = 96

        for (let i = 0; i < 7; i++) {
            let text = lastWeek[i]
            text = text.slice(5)
    
            this.drawText(currentX, baselineY, text)
            
            currentX += 40
        }
    }

    // Get data to display by selected time window
    getExpenseData(timeWindow){
        let maxExpense = 0
        let lastWeek

        if(timeWindow === "last-week"){
            lastWeek = getLastWeekDates()
        }else{
            lastWeek = getLast7DaysDates()
        }
        this.generateSVGDays(lastWeek)
        
        let data = new Array(7);
        data.fill(0, 0, 8)

        let expenses = JSON.parse(localStorage.getItem("expenses"))
        if (expenses !== null) {
            expenses.forEach((expense) => {
                let index = lastWeek.indexOf(expense.date)
                if (index != -1) {
                    data[index] += parseInt(expense.cost)
                    maxExpense = Math.max(maxExpense, expense.cost)
                }
            })
        }
        return {data, maxExpense}
    }

    drawRectangle(x, y, width, height) {
        const rectangle = document.createElementNS(this.svgNs, "rect");
        rectangle.setAttributeNS(null, "x", x);
        rectangle.setAttributeNS(null, "y", y);
        rectangle.setAttributeNS(null, "width", width);
        rectangle.setAttributeNS(null, "height", height);
        rectangle.setAttributeNS(null, "fill", "blue");
        this.svg.appendChild(rectangle);
    }

    drawText(x, y, text) {
        const svgText = document.createElementNS(this.svgNs, "text");
        svgText.setAttributeNS(null, "x", x);
        svgText.setAttributeNS(null, "y", y);
        svgText.innerHTML = text
        svgText.classList.add("small")
        this.svg.appendChild(svgText)
    }

    clickHandler(e) {
        const x = e.offsetX;
        const y = e.offsetY;

        if (e.ctrlKey || e.metaKey) {
            this.createPath();
            this.draw(x, y);
        } else if (e.shiftKey) {
            this.clear();
        } else {
            if (!this.path) {
                this.createPath();
            }
            this.draw(x, y);
        }
    }

    draw(x, y) {
        let d = this.path.getAttributeNS(null, "d");
        if (!d) {
            d = `M ${x} ${y}`;
        } else {
            d += ` L ${x} ${y}`;
        }

        console.log("");
        console.log("d", d);

        this.path.setAttributeNS(null, "d", d);
        this.drawCircle(x, y);
    }

    clear() {
        this.svg.innerHTML = "";
        this.path = null;
    }

    drawCircle(x, y) {
        const circle = document.createElementNS(this.svgNs, "circle");
        circle.setAttributeNS(null, "cx", x);
        circle.setAttributeNS(null, "cy", y);
        circle.setAttributeNS(null, "r", "2");
        circle.setAttributeNS(null, "fill", "red");
        this.svg.appendChild(circle);
    }

    createPath() {
        const path = document.createElementNS(this.svgNs, "path");
        path.setAttributeNS(null, "stroke", "black");
        path.setAttributeNS(null, "stroke-width", "6px");
        path.setAttributeNS(null, "stroke-linejoin", "round");
        path.setAttributeNS(null, "fill", "transparent");

        this.svg.appendChild(path);
        this.path = path;
    }
}