import { initMobileNav } from "./modules/commonRoutines.js";
import { PageDashboard, PageExpenses, PageLanding, PageFile, PageNotFound, PageAbout } from "./modules/pageModule.js"

class Router {
    constructor({ pages, defaultPage }) {
        this.pages = pages;
        this.defaultPage = defaultPage;
        this.currentPage = null

        // First run
        this.route(window.location.href);

        // Listen on url changes from user clicking back button
        window.addEventListener('popstate', e => {
            this.route(window.location.href);
        });

        // Listen on url changes from user clicks, only nav
        document.querySelectorAll("a[class~='router-url']").forEach((element) => {
            element.addEventListener("click", e => {
                const element = e.target
                if (element.nodeName === 'A') {
                    e.preventDefault();
                    this.route(element.href);
                    window.history.pushState(null, null, element.href)
                    this.setNavElementActive(element)
                }
            })
        })

        // Checks internet connection
        this.registerInternetConnectionListeners()
    }

    route(urlString) {
        const url = new URL(urlString)
        const page = url.searchParams.get('page')

        if (this.currentPage) {
            this.currentPage.pageHide()
        }

        const page404 = this.pages.find(p => p.key === '404')
        const pageInstanceMatched = this.pages.find(p => p.key === (page ?? this.defaultPage))
        const currentPage = pageInstanceMatched ?? page404

        this.currentPage = currentPage
        this.currentPage.pageShow()

        this.setAllInactive()
        if (page === null) {
            this.setDefaultActive()
        } else {
            this.setNavElementActive(document.querySelector("a[href*='" + page + "']"))
        }
    }

    setDefaultActive() {
        let element = document.querySelector("a[href*='landing-page']")
        element.classList.add("active")
    }

    setNavElementActive(element) {
        this.setAllInactive()
        element.classList.add("active");
    }

    setAllInactive() {
        let elements = document.querySelectorAll(".router-url")
        elements.forEach(element => {
            element.classList.remove("active")
        });
    }

    registerInternetConnectionListeners() {

        window.addEventListener("offline", this.displayNoIneternetConnection);
    
        window.addEventListener("online", this.hiddeNoInternetConnection);
    }

    displayNoIneternetConnection(){
        alert("No internet connection, don't worry, application is still fully usable without internet")
    }

    hiddeNoInternetConnection(){
        alert("Internet connection is back")
    }
}

new Router({
    pages: [
        new PageLanding({ key: 'landing-page', title: 'Landing page' }),
        new PageFile({ key: 'file-page', title: 'File page' }),
        new PageDashboard({ key: 'dashboard-page', title: 'Dashboard page' }),
        new PageExpenses({ key: 'expense-page', title: 'Expense page' }),
        new PageAbout({key: 'about-page', title: 'About page'}),
        new PageNotFound({ key: '404', title: 'Page not found' })
    ],
    defaultPage: 'landing-page'
});

// Change nav for mobile version
initMobileNav()