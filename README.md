## KAJ Expense tracker

Aplikace na krátkodobé monitorovanání spíše menších finančních útrat.

### Obsah

Aplikace obsahuje 5 stránek.

1. Landing page - prezentační stránka
2. Dashboard - zobrazí graf a údaje o útratách
3. Expense page - slouží k přidávání a odebírání útrat
4. File page - účelem je import a export dat v podobě .cvs souboru
5. About page - stránka s informacemi o aplikaci v podobě vide a zvuku, pouze dummy informace

## Funkčnost

Použití aplikace je jednoduché. Stačí vložit data o útratách pomocí stránky Expanse page, tedy pomocí formuláře, nebo lze také data importovat .csv souborem. Formát csv je název útraty, cena útraty, datum(YYYY-mm-dd). Data lze prohlížet na stránce Dashboard. Aplikace podporuje i export dat do .csv souboru. Data jsou uložena v local storage, proto zůstanou uložená, dokud je někdo z prohlížeče neodstraní.

## Postup

Aplikaci jsem začal budovat od hlavních funkcionalit, až poté jsem se zaměřil na design a detaily.

